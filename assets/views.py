from assets.models import Asset
from assets.serializers import AssetSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from assets.serializers import UserSerializer
from rest_framework import permissions
from assets.permissions import IsOwnerOrReadOnly
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework.response import Response
from django.http import HttpResponse
from PIL import Image
from rest_framework import status
import os
from django.template import RequestContext, Template, Context
from django.template import loader

def cmod_image(request, pk=1, type="png", h="256", w="256"):
    try:
        asset = Asset.objects.get(pk=pk)
    except Asset.DoesNotExist:
        return HttpResponse("Asset ID " + pk + " does not exist", status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
    	x =  asset.pic
    	im = Image.open(x.path)
    	infile = x.path
    	f, e = os.path.splitext(infile)
        outfile = f + "." + type
        im.save(outfile, type)
        template = loader.get_template('assets/casset.html')
        context = {
            'img': x.name.split(".")[0] + "." + type,
            'json': asset.json,
            'h': h,
            'w': w,
            'comments': asset.comments,
            'type': type,
        }
        return HttpResponse(template.render(context, request))
    	
def mod_image(request, pk=1, type="png", h="256", w="256"):
    try:
        asset = Asset.objects.get(pk=pk)
    except Asset.DoesNotExist:
        return HttpResponse("Asset ID " + pk + " does not exist", status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
    	x =  asset.pic
    	im = Image.open(x.path)
    	infile = x.path
    	f, e = os.path.splitext(infile)
        outfile = f + "." + type
        im.save(outfile, type)
    	html = Template('<img src="../../../'+ x.name.split(".")[0] + "." + type + '" height="'+h+'" width="'+w+'" alt="Image" />')
    	return HttpResponse(html.render(RequestContext(request)))

def orig_image(request, name):
    try:
	    image_data = open("./images/" + name, "rb").read()
	    return HttpResponse(image_data, content_type="image/jpg")
    except:
	    return HttpResponse("Image not found",status=404)

class AssetHighlight(generics.GenericAPIView):
    queryset = Asset.objects.all()
    renderer_classes = (renderers.StaticHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        asset = self.get_object()
        return Response(asset.highlighted)

@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'assets': reverse('asset-list', request=request, format=format)
    })

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AssetList(generics.ListCreateAPIView):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    def perform_create(self, serializer):
	    serializer.save(owner=self.request.user)


class AssetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                      IsOwnerOrReadOnly,)
