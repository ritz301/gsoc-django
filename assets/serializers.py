from rest_framework import serializers
from assets.models import Asset, LANGUAGE_CHOICES, STYLE_CHOICES
from django.contrib.auth.models import User

class UserSerializer(serializers.HyperlinkedModelSerializer):
    assets = serializers.HyperlinkedRelatedField(many=True, view_name='asset-detail', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'assets')

class AssetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    highlight = serializers.HyperlinkedIdentityField(view_name='asset-highlight', format='html')

    class Meta:
        model = Asset
        fields = ('id', 'title', 'comments', 'json', 'pic', 'linenos', 'language', 'style','owner', 'highlight')
