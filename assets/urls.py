from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from assets import views

# API endpoints
urlpatterns = format_suffix_patterns([
    url(r'^$', views.api_root),
    url(r'^assets/$',
        views.AssetList.as_view(),
        name='asset-list'),
    url(r'^assets/(?P<pk>[0-9]+)/$',
        views.AssetDetail.as_view(),
        name='asset-detail'),
    url(r'^assets/(?P<pk>[0-9]+)/highlight/$',
        views.AssetHighlight.as_view(),
        name='asset-highlight'),
    url(r'^assets/(?P<pk>[0-9]+)/image__(?P<type>(jpe?g|png|gif|bmp)+)/(?P<h>[0-9]+)/(?P<w>[0-9]+)$',
        views.mod_image,
        name='asset-modimage'),
    url(r'^assets/(?P<pk>[0-9]+)/cimage__(?P<type>(jpe?g|png|gif|bmp)+)/(?P<h>[0-9]+)/(?P<w>[0-9]+)$',
        views.cmod_image,
        name='asset-cmodimage'),
    url(r'^assets/images/(?P<name>[_.A-Za-z0-9]+)$',
        views.orig_image,
        name='asset-origimage'),
    url(r'^users/$',
        views.UserList.as_view(),
        name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)/$',
        views.UserDetail.as_view(),
        name='user-detail')
])

# Login and logout views for the browsable API
urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]
