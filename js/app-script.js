startScript("canvas1");

function startScript(canvasId)
{ 
	var isRecording = false;
	playbackInterruptCommand = "";
	
	$(document).bind("ready", function()
	{
		drawing = new RecordableDrawing(canvasId);
		alert("Recording started");
		startRecording();

		$("#submitt").click(function(){
			if($('#textarea1').val().trim().length > 0){
				alert($('#textarea1').val());
				alert("Recording stopped");
				stopRecording();
			}
			else{
				alert("Enter some comment");
			}
		});
		
		function playRecordings()
		{
			if (drawing.recordings.length == 0)
			{
				alert("No recording to play");
				return;
			}
			startPlayback();			
		}
		
		$("#clearBtn").click(function(){
			drawing.recordings = new Array()
			drawing.currentRecording = null;
			startRecording();
			var canvas,ctx;
			canvas = document.getElementById("canvas1");
			ctx = canvas.getContext("2d");
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			// drawing.clearCanvas();			
		});
	
		$("#serializeBtn").click(function() {
			var serResult = serializeDrawing(drawing);
			if (serResult != null)
			{
				$("#serDataTxt").val(serResult);
				// showSerializerDiv();
			} else
			{
				alert("Error serializing data");
			}
		});

		function showSerializerDiv(showSubmit)
		{
			$("#drawingDiv").hide();
			$("#canvasBtnsDiv").hide();
			$("#serializerDiv").show();	
			if (showSubmit)
				$("#okBtn").show();
			else
				$("#okBtn").hide();
		}

		function hideSerializerDiv()
		{
			$("#drawingDiv").show();
			$("#canvasBtnsDiv").show();
			$("#serializerDiv").hide();	
		}

		$("#deserializeBtn").click(function(){
			showSerializerDiv(true);
		});

		$("#cancelBtn").click(function(){
			hideSerializerDiv();
		});

		$("#okBtn").click(function(){
			startRecording();
			var serTxt = $("#serDataTxt").val();
			var result = deserializeDrawing(serTxt);
			if (result == null)
				result = "Error : Unknown error in deserializing the data";
			if (result instanceof Array == false)
			{
				$("#serDataTxt").val(result.toString());
				// showSerializerDiv(false);
				return;
			} 
			else
			{
				//data is successfully deserialize
				drawing.recordings = result;
				//set drawing property of each recording
				for (var i = 0; i < result.length; i++)
					result[i].drawing = drawing;
				// hideSerializerDiv();
				playRecordings();
			}
		});
		
		$("#colorsDiv .colorbox").click(function(){
			$("#colorsDiv .colorbox").removeClass("selectedColor");
			$(this).addClass("selectedColor");
			drawing.setColor($(this).css("background-color"));
		});
		
		$(".stroke").click(function(){
			$(".stroke_selected").removeClass("stroke_selected");
			$(this).addClass("stroke_selected");
			var size = $(this).css("border-radius");
			drawing.setStokeSize(parseInt(size));
		});
		
		var size = parseInt($(".stroke_selected").css("border-radius"));
		if (size > 0)
			drawing.setStokeSize(size);
	});
	
	function stopRecording()
	{
		$("#clearBtn").show();
		
		drawing.stopRecording();
		isRecording = false;
	}
	
	function startRecording()
	{
		// $("#clearBtn").hide();
		
		drawing.startRecording();
		isRecording = true;
		//set curent color
		var color = $("#colorsDiv .selectedColor").css("background-color");
		var strokesize = parseInt($(".stroke_selected").css("border-radius"));
		drawing.setColor(color);
		drawing.setStokeSize(strokesize);
	}
	
	function stopPlayback()
	{
		playbackInterruptCommand = "stop";		
	}
	
	function startPlayback()
	{
		var currColor = $("#colorsDiv .selectedColor").css("background-color");
		var currStrokeSize = parseInt($(".stroke_selected").css("border-radius"));
		
		drawing.playRecording(function() {
			//on playback start
			// $("#clearBtn").hide();
			playbackInterruptCommand = "";
		}, function(){
			//on playback end
			$("#clearBtn").show();
			if (currColor && currColor != "")
				drawing.setColor(currColor);
			if (currStrokeSize > 0)
				drawing.setStokeSize(currStrokeSize);
		}, function() {
			//on pause
			// $("#clearBtn").hide();
		}, function() {
			//status callback
			return playbackInterruptCommand;
		});
	}
	
	function pausePlayback()
	{
		playbackInterruptCommand = "pause";
	}
	
	function resumePlayback()
	{
		playbackInterruptCommand = "";
		drawing.resumePlayback(function(){
			// $("#clearBtn").hide();
		});
	}
	
	function pauseRecording()
	{
		drawing.pauseRecording();
	}
	
	function resumeRecording()
	{
		drawing.resumeRecording();
	}
}
